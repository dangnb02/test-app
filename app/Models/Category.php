<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name',
    ];
    protected $table = 'categories';
    public function getCAtegories()
    {
        $categories = DB::table('categories')->get();
        return $categories;
    }
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }
    use HasFactory;
}
