<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{

    protected $table = 'posts';
    public function getAllPostAndCategory()
    {
        // $posts = DB::table('posts')->get();
        $posts = DB::table('posts')->leftJoin('category_post', 'posts.id', '=', 'category_post.post_id')
            ->leftJoin('categories', 'category_post.category_id', '=', 'categories.id')->select(
                'posts.*',
                DB::raw('GROUP_CONCAT(categories.name SEPARATOR \',\') as category_name',),
                // 'category_post.post_id as postid',
                // 'categories.id as category_id',
            )->groupBy('posts.id')->get();

        return $posts;
    }


    public function getAllCategory()
    {
        $categories = DB::table('categories')->get();
        return $categories;
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }
    use HasFactory;
}
