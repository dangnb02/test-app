<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category_post extends Model
{
    protected $table = 'category_post';
    public $timestamps = false;
    use HasFactory;
}
