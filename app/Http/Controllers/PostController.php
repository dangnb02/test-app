<?php

namespace App\Http\Controllers;

use App\Models\Category_post;
use App\Models\Category;
use App\Models\Post;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    private $posts;
    public function __construct()
    {
        $this->posts = new Post();
    }
    public function index()
    {
        $title = "Tất cả bài viết";

        $postList = $this->posts->getAllPostAndCategory();
        return view("posts.view", compact('postList', 'title'));
    }

    public function add()
    {
        $title = 'Thêm bài viết';
        $categoryList = $this->posts->getAllCategory();
        return view('posts.add', compact('categoryList', 'title'));
    }
    public function create(Request $request)
    {
        $data = new Post();
        $data->name = $request->name;
        $file = $request->thumbnail;
        $filename = date('YmdHi') . $file->getClientOriginalName();
        $data->thumbnail = $filename;
        $file->move(public_path('files'), $filename);
        $data->slug = $request->slug;
        $data->description = $request->description;
        $data->content = $request->content;
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        if (!empty($request->categories)) {
            $list = explode(',', $request->categories);
            foreach ($list as $item) {

                $post_cate = new Category_post();
                $post_cate->category_id = $item;
                $post_cate->post_id = $data->id;
                $post_cate->save();
            }
        }
        return redirect()->route('posts.index');
    }

    public function edit($id, Request $request)
    {
        $title = 'Sửa bài viết';
        $post = Post::findOrFail($id);
        $categoryList = $this->posts->getAllCategory();
        $allPostAndCate = $this->posts->getAllPostAndCategory();
        return view('posts.edit', compact('post', 'categoryList', 'allPostAndCate', 'title'));
    }
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->name = $request->name;
        $file = $request->thumbnail;
        $filename = date('YmdHi') . $file->getClientOriginalName();
        $post->thumbnail = $filename;
        $file->move(public_path('files'), $filename);
        $post->slug = $request->slug;
        $post->description = $request->description;
        $post->content = $request->content;
        $post->created_at = date('Y-m-d H:i:s');
        $post->save();
        if (!empty($request->categories)) {
            $list = explode(',', $request->categories);
            foreach ($list as $item) {
                $post_cate = Category_post::findOrFail($id);
                $post_cate->category_id = $item;
                $post_cate->post_id = $id;
                $post_cate->save();
            }
        };
        return redirect()->route('posts.index');
    }
    public function delete($id)
    {
        Post::destroy($id);
        return redirect()->route('posts.index');
    }
}
