<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categories;
    public function __construct()
    {
        $this->categories = new Category();
    }
    public function index()
    {
        $title = 'Danh mục';
        $categoryList = $this->categories->getCAtegories();

        return view('categories.view', compact('title', 'categoryList'));
    }
    public function add()
    {
        $title = 'Thêm danh mục';
        return view('categories.add', compact('title'));
    }
    public function create(Request $request)
    {
        $data = new Category();
        $data->name = $request->name;
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $title = 'Sửa danh mục';
        $cate = Category::findOrFail($id);
        return view('categories.edit', compact('cate', 'title'));
    }
    public function update(Request $request, $id)
    {
        $cate = Category::findOrFail($id);
        $data = $request->all();
        $cate->update($data);
        return redirect()->route('categories.index');
    }
    public function delete($id)
    {
        Category::destroy($id);
        return redirect()->route('categories.index');
    }
}
