<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $users;

    public function __construct()
    {
        $this->users = new User();
    }
    public function index()
    {
        $title = 'Quản lý User';
        $userList = $this->users->getUser();

        return view('users.view', compact('userList', 'title'));
    }
    public function add()
    {
        $title = 'Thêm user';
        return view('users.add', compact('title'));
    }
    public function create(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->created_at = date('Y-m-d H:i:s');
        $user->save();
        return redirect()->route('users.index');
    }
    public function edit($id)
    {
        $title = 'Sửa User';
        $user = User::findOrFail($id);
        return view('users.edit', compact('user', 'title'));
    }
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->created_at = date('Y-m-d H:i:s');
        $user->save();
        return redirect()->route('users.index', 'title');
    }
    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index');
    }
}
