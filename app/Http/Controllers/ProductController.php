<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    private $products;

    public function __construct()
    {
        $this->products = new Product();
    }
    public function index()
    {
        $title = "Tất cả Sản phẩm";
        $productList = $this->products->getProduct();
        return view("products.view", compact("title", "productList"));
    }
    public function listProduct()
    {
        $title = "Danh sách Sản phẩm";
        $productList = $this->products->getProduct();
        return view("products.lists", compact("title", "productList"));
    }
    public function add()
    {
        $title = "Thêm sản phẩm";
        return view("products.add", compact("title"));
    }
    public function create(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $file = $request->image;
        $filename = date('YmdHi') . $file->getClientOriginalName();
        $product->image = $filename;
        $file->move(public_path('files'), $filename);
        $product->price = $request->price;
        $product->description = $request->description;
        $product->save();
        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $title = 'Sửa Sản phẩm';
        $product = Product::findOrFail($id);
        return view('products.edit', compact('title', 'product'));
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $file = $request->image;
        $filename = date('YmdHi') . $file->getClientOriginalName();
        $file->move(public_path('files'), $filename);
        $product->image = $filename;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->save();
        return redirect()->route('products.index');
    }


    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index');
    }
}
