@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<h3 class="d-flex justify-content-center">{{ $title }}</h3>

@if ($errors->any())
<div class="alert alert-danger">Dữ liệu nhập vào không hợp lệ</div>
@endif
<div class="container">
    <form method="POST">
        @csrf
        <div class="mb-3">
            <div class="form-group">
                <label for="name">Tên Danh mục</label>
                <input type="text" class="form-control" id="name" name="name" required value="{{ $cate->name }}">
            </div>

            <button type="submit" class="btn btn-primary">Cập nhật</button>
            <a href=" {{ route('categories.index') }} " class="btn btn-warning">Quay lại</a>
    </form>
</div>

@endsection
