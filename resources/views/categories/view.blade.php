@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<div class="container">
    <h2 class="d-flex justify-content-center">{{$title}}</h2>
<a href=" {{ route('categories.add') }} " class="btn btn-primary my-2">Thêm Danh mục</a>
<a href=" {{ route('home') }} " class="btn btn-warning">Quay lại</a>
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="5%">STT</th>
            <th>Tên danh mục</th>
            <th>Ngày tạo</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($categoryList))
        @foreach($categoryList as $key => $item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->created_at}}</td>
            <td>
                <a href="{{ route('categories.edit', ['id'=>$item->id]) }}" class="btn btn-primary">Sửa</a>
                <a href=" {{ route('categories.delete', ['id'=>$item->id]) }}" class="btn btn-danger">Xóa</a>
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td> Không có danh mục</td>
        </tr>
        @endif
    </tbody>
</table>
</div>
@endsection
