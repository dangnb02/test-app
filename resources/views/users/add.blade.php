@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')

<div class="container">
    <h3>{{$title}}</h3>
    <form method="post" >
        @csrf
        <div class="mb-3">
            <div class="form-group">
                <label for="name">Tên người dùng</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Mật khẩu</label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Thêm mới</button>
        <a href=" {{ route('users.index') }} " class="btn btn-warning">Quay lại</a>
    </form>
</div>
@endsection
