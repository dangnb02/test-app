@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')

<div class="container">
    <h3>{{$title}}</h3>
    <a href=" {{ route('users.add') }} " class="btn btn-primary my-2">Thêm User</a>
    <a href=" {{ route('home') }} " class="btn btn-warning">Quay lại</a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>STT</th>
                <th>Tên người dùng</th>
                <th>Email</th>
                <th>Password</th>
                <th>Ngày tạo</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($userList))
            @foreach($userList as $key => $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->password }}</td>
                <td>{{ $item->created_at }}</td>
                <td>
                    <a href="{{ route('users.edit', ['id'=>$item->id]) }}" class="btn btn-primary">Sửa</a>
                    <a href="{{ route('users.delete', ['id'=>$item->id]) }}" class="btn btn-danger">Xóa</a>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>Không có dữ liệu</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
