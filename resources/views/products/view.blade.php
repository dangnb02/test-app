@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<div class="container">
    <h2 class="d-flex justify-content-center">{{$title}}</h2>
<a href=" {{ route('products.add') }} " class="btn btn-primary my-2">Thêm Sản phẩm</a>
<a href=" {{ route('home') }} " class="btn btn-warning">Quay lại</a>
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="5%">STT</th>
            <th>Tên Sản phẩm</th>
            <th>Hình ảnh</th>
            <th>Giá</th>
            <th>Mô tả</th>
            <th>Ngày tạo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($productList))
        @foreach($productList as $key => $item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->name}}</td>
            <td><img src="/files/{{$item->image}}" alt=" {{ $item->name }} " width="100px"></td>

            <td>{{ $item->price }}đ</td>

            <td>{{$item->description}}</td>
            <td>{{$item->created_at}}</td>
            <td>
                <a href="{{ route('products.edit', ['id'=>$item->id]) }}" class="btn btn-primary">Sửa</a>
                <a href="{{ route('products.delete', ['id'=>$item->id]) }}" class="btn btn-danger">Xóa</a>
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td> Khong co nguoi dung</td>
        </tr>
        @endif
    </tbody>
</table>
</div>
@endsection
