@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<div class="container">
    <h2 class="d-flex justify-content-center">{{$title}}</h2>
    <a href=" {{ route('home') }} " class="btn btn-warning my-2">Quay lại</a>
    <div class="row flex">
        @if(!empty($productList))
        @foreach ($productList as $item)
        <div class="col-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">{{$item->name}}</h5>
                  <img src="/files/{{$item->image}}" width="100px" height="100px">
                  <h6 class="card-subtitle my-2 text-muted">{{$item->price}}đ</h6>
                  <a href="#" class="card-link">Thêm vào giỏ hàng</a>
                  <a href="#" class="card-link">Chi tiết</a>

                </div>
            </div>
        </div>
        @endforeach
        @else
        <p>Khong co san pham</p>
        @endif
    </div>
</div>
@endsection
