@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')

<div class="container">
    <h3>{{$title}}</h3>
    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <div class="form-group">
                <label for="name">Tên Sản phẩm</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="image">Hình ảnh</label>
                <input type="file" class="form-control" id="image" name="image" required accept="image/*">
            </div>
            <div class="form-group">
                <label for="price">Giá</label>
                <input type="number" class="form-control" id="price" name="price" required>
            </div>
            <div class="form-group">
                <label for="description">Mô tả</label>
                <input type="text" class="form-control" id="description" name="description" required>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Thêm mới</button>
        <a href=" {{ route('products.index') }} " class="btn btn-warning">Quay lại</a>
    </form>
</div>
@endsection
