@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<h3 class="d-flex justify-content-center">{{ $title }}</h3>

@if ($errors->any())
<div class="alert alert-danger">Dữ liệu nhập vào không hợp lệ</div>
@endif
<div class="container">
    <form method="POST" enctype="multipart/form-data">
        <div class="mb-3">
            <div class="form-group">
                <label for="name">Tên bài viết</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="thumbnail">Hình ảnh</label>
                <input type="file" class="form-control" id="thumbnail" name="thumbnail" required accept="image/*">
            </div>
            <div class="form-group">
                <label for="slug">Slug</label>
                <textarea class="form-control" id="slug" name="slug"></textarea>
            </div>
            <div class="form-group">
                <label for="description">Mô tả</label>
                <textarea class="form-control" id="description" name="description" required></textarea>
            </div>
            <div class="form-group">
                <label for="content">Nội dung</label>
                <textarea class="form-control" name="content" id="content"></textarea>
            </div>
            <div class="form-group">
                <label for="categories">Danh mục</label>

                <div class="form-check px-6">
                    <select name="slcategories" id="slcategories" onchange="onChange()" class="js-example-basic-multiple" multiple="multiple">
                        @foreach ($categoryList as $category)
                        <option value="{{ $category->id }}">{{ $category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" class="form-control" id="categories" name="categories">
            </div>
        </div>
        @csrf
        <button type="submit" class="btn btn-primary">Tạo bài viết</button>
        <a href=" {{ route('posts.index') }} " class="btn btn-warning">Quay lại</a>
    </form>
</div>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '50%'
        });
    });

    function onChange() {
        var value = $("#slcategories").val()
        $("#categories").val(value.toString());
    }
</script>
@endsection
