@extends('home')
@section('title')
{{$title}}
@endsection
@section('content')
<div class="container">
    <h2 class="d-flex justify-content-center">{{$title}}</h2>
<a href=" {{ route('posts.add') }} " class="btn btn-primary my-2">Thêm bài viết</a>
<a href=" {{ route('home') }} " class="btn btn-warning">Quay lại</a>
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="5%">STT</th>
            <th>Tên bài viết</th>
            <th>Hình ảnh</th>
            <th>Chuyên mục</th>
            <th>Mô tả</th>
            <th>Ngày tạo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($postList))
        @foreach($postList as $key => $item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->name}}</td>
            <td><img src=" files/{{ $item->thumbnail  }}" alt=" {{ $item->name }} " width="100px"></td>

            <td>{{ $item->category_name }}</td>

            <td>{{$item->description}}</td>
            <td>{{$item->created_at}}</td>
            <td>
                <a href="{{ route('posts.edit', ['id'=>$item->id]) }}" class="btn btn-primary">Sửa</a>
                <a href="{{ route('posts.delete', ['id'=>$item->id]) }}" class="btn btn-danger">Xóa</a>
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td> Khong co nguoi dung</td>
        </tr>
        @endif
    </tbody>
</table>
</div>
@endsection
