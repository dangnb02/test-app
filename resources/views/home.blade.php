@extends('layouts.app')

@section('content')
<div class="container">
    <header class="container">
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Trang chủ</a>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item ">
                <a class="nav-link active" aria-current="page" href="{{ route("posts.index") }}">Trang bài viết</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link active" aria-current="page" href="{{ route("categories.index") }}">Trang Danh mục</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link active" aria-current="page" href="{{ route("users.index") }}">Trang User</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link active" aria-current="page" href="{{ route("products.index") }}">Trang Sản phẩm</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link active" aria-current="page" href="{{ route("products.lists") }}">Trang chính</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container">
        <div class="content">
            @yield('content')
        </div>
    </div>
    <footer class="container d-flex justify-content-end">
        Copyright: 2024
    </footer>
</div>
@endsection
