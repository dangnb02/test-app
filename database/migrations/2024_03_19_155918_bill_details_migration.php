<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("bill_details", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("product_id");
            $table->integer("product_quantity");
            $table->unsignedInteger("bill_id");
            $table->foreign("product_id")->references("id")->on("products");
            $table->foreign("bill_id")->references("id")->on("bills");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("build_details");
    }
};
